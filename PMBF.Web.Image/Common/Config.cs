﻿using Furion;

namespace PMBF.Web.Image.Common
{
    public class Config
    {
        public static string SaveBasePath = App.Configuration["SaveImageBasePath"];

        //用户头像文件夹
        public static string HeaderSaveBasePath = "Header";

        //用户头像备份文件夹
        public static string HeaderBackSaveBasePath = "Header";

        public const string MIME = "image/jpeg";

        /// <summary>
        /// 文件夹存放深度
        /// </summary>
        public const int SaveDeep = 4;

        /// <summary>
        /// 每层文件夹深度
        /// </summary>
        public const int NumPerDeep = 100;
    }
}