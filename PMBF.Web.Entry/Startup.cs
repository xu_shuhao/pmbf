using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PMBF.Web.Entry
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //公共配置转移到Core项目统一配置
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //公共配置转移到Core项目统一配置
        }
    }
}
