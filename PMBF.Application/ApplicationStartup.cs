﻿using System;
using System.Net.Http;
using System.Net.Sockets;
using Furion;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace PMBF.Application
{
    /// <summary>
    /// 网络相关配置
    /// </summary>
    public class ApplicationStartup : AppStartup
    {
        /// <summary>
        /// service manage
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRemoteRequest(options =>
            {
                // 配置默认客户端
                _ = options.AddHttpClient("default", c =>
                  {
                      c.BaseAddress = new Uri("http://101.201.238.44:88/");
                      c.DefaultRequestHeaders.Add("param", "zi ding yi!");
                  });

                // 配置 第三方 基本信息
                options.AddHttpClient("thirweb", c =>
                {
                    c.BaseAddress = new Uri("http://ck.wiscoor.com/");
                    c.DefaultRequestHeaders.Add("wiscoor", "zi ding yi!");
                });

                //微信 
                options.AddHttpClient("weixin", c =>
                {
                    c.BaseAddress = new Uri("https://api.weixin.qq.com/");
                });
            });
        }

        /// <summary>
        /// null
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
        }

    }
}
