﻿using System;

namespace PMBF.Application.Http3thApi.WX.Models
{
    public class Code2SessionResult
    {

        /// <summary>
        /// openid string 用户唯一标识
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// session_key string 会话密钥
        /// </summary>
        public string session_key { get; set; }

        /// <summary>
        /// unionid string 用户在开放平台的唯一标识符，若当前小程序已绑定到微信开放平台帐号下会返回，详见 UnionID 机制说明。
        /// </summary>
        public string unionid { get; set; }

        /// <summary>
        /// errcode number  错误码
        /// </summary>
        public WXErrorCode errcode { get; set; }

        /// <summary>
        /// errmsg  string 错误信息
        /// </summary>
        public string errmsg { get; set; }

    }
}
