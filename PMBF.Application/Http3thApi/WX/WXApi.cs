﻿using System.Threading.Tasks;
using PMBF.Application.Http3thApi.WX.Models;
using Furion.RemoteRequest;

namespace PMBF.Application.Http3thApi.WX
{
    public interface WXApi : IHttpDispatchProxy
    {
        /// <summary>
        /// 根据客户端code获取openid
        /// </summary>
        /// <param name="APPID"></param>
        /// <param name="SECRET"></param>
        /// <param name="JSCODE"></param>
        /// <param name="authorization_code"></param>
        /// <returns></returns>
        [Get("sns/jscode2session?appid={APPID}&secret={SECRET}&js_code={JSCODE}&grant_type={authorization_code}"), Client("weixin")]
        Task<Code2SessionResult> GetWxOpenId(string APPID, string SECRET, string JSCODE, string authorization_code);

    }
}
