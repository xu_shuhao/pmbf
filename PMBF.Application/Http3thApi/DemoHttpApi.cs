﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Furion.JsonSerialization;
using Furion.RemoteRequest;
using Furion.UnifyResult;
using PMBF.Core.ViewModels;

namespace PMBF.Application.Http3thApi
{
    /// <summary>
    /// Http demo
    /// </summary>
    public interface IHttp : IHttpDispatchProxy, IJsonSerializerProvider
    {

        [Post("api/rbac/login"), Client("default")]
        Task<RESTfulResult<object>> PostAAsync([Body("application/json")] UserViewModel user);


        [Get("sapp/system/getversion"), Client("thirweb")]
        Task<string> GetBAsync();

        // 全局拦截，类中每一个方法都会触发
        [Interceptor(InterceptorTypes.Request)]
        static void OnRequesting1(HttpRequestMessage req)
        {
            //req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            req.Headers.Add("Authorization", value: "Bearer abc.dd.ccc");
        }
    }
}
