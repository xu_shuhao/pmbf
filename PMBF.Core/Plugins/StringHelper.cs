﻿using System;
using System.Text.RegularExpressions;

namespace PMBF.Core.Plugins
{
    public static class StringHelper
    {
        public static string ConvertClassNameString(string tb1)
        {
            tb1 = tb1.ToLower();
            char charSplit = '_';
            Match mt = Regex.Match(tb1, @"_(\w*)*");
            while (mt.Success)
            {
                var item = mt.Value;
                while (item.IndexOf('_') >= 0)
                {
                    string newUpper = item.Substring(item.IndexOf(charSplit), 2);
                    item = item.Replace(newUpper, newUpper.Trim(charSplit).ToUpper());
                    tb1 = tb1.Replace(newUpper, newUpper.Trim(charSplit).ToUpper());
                }
                mt = mt.NextMatch();
            }

            tb1 = tb1.Substring(0, 1).ToUpper() + tb1.Substring(1, tb1.Length-1);

            return tb1;
        }

        /// <summary>
        /// 字符串MD5加密
        /// </summary>
        /// <param name="Text">要加密的字符串</param>
        /// <returns>32位密文</returns>
        public static string MD5(string Text)
        {
            byte[] buffer = System.Text.Encoding.Default.GetBytes(Text);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider check;
                check = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] somme = check.ComputeHash(buffer);
                string ret = "";
                foreach (byte a in somme)
                {
                    if (a < 16)
                        ret += "0" + a.ToString("X");
                    else
                        ret += a.ToString("X");
                }
                return ret.ToLower();
            }
            catch
            {
                throw;
            }
        }

        public static string GeneratePassword(string password)
        {
            string _password = MD5(password);
            _password = _password.Substring(16, 16) + _password.Substring(0, 16);
            _password = _password.Replace('0', 'x');
            _password = MD5(_password);
            return _password;
        }

    }
}
