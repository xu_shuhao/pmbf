﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace PMBF.Core.ViewModels
{
    public class LoginResult
    {
        [JsonPropertyName("usercode")]
        [DefaultValue("admin")]
        public string Usercode { get; set; }

        [JsonPropertyName("password")]
        [DefaultValue("111111")]
        public string Password { get; set; }

        [JsonPropertyName("autoLogin")]
        public bool AutoLogin { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("clientType")]
        public string ClientType { get; set; }
    }
}
