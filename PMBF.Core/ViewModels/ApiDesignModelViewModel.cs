﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PMBF.Core.ViewModels
{

    public class Field
    {
        [JsonPropertyName("name")]
        public string Name;

        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("editDisabled")]
        public bool EditDisabled;

        [JsonPropertyName("data")]
        public string Data;

        [JsonPropertyName("requiredInForm")]
        public bool RequiredInForm;

        [JsonPropertyName("hideInForm")]
        public bool HideInForm;

        [JsonPropertyName("hideInColumn")]
        public bool? HideInColumn;

        [JsonPropertyName("isLike")]
        public bool? IsLike;

    }

    public class EditAction
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;
    }

    public class TableToolbar
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;
    }

    public class BatchToolbar
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;
    }

    public class BatchToolbarTrashed
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("method")]
        public string Method;

        [JsonPropertyName("uri")]
        public string Uri;
    }

    public class ListAction
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;
    }

    public class AddAction
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;
    }

    public class ApiDesignModel
    {
        [JsonPropertyName("id")]
        public int Id;

        [JsonPropertyName("routeName")]
        public string RouteName;

        [JsonPropertyName("menuName")]
        public string MenuName;

        [JsonPropertyName("formColumns")]
        public int FormColumns;

        [JsonPropertyName("fields")]
        public List<Field> Fields;

        [JsonPropertyName("editAction")]
        public List<EditAction> EditAction;

        [JsonPropertyName("tableToolbar")]
        public List<TableToolbar> TableToolbar;

        [JsonPropertyName("batchToolbar")]
        public List<BatchToolbar> BatchToolbar;

        [JsonPropertyName("batchToolbarTrashed")]
        public List<BatchToolbarTrashed> BatchToolbarTrashed;

        [JsonPropertyName("listAction")]
        public List<ListAction> ListAction;

        [JsonPropertyName("addAction")]
        public List<AddAction> AddAction;
    }




}
