﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PMBF.Core.ViewModels
{
    public class Page
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("searchBar")]
        public bool SearchBar;

        [JsonPropertyName("trash")]
        public bool Trash;

        [JsonPropertyName("tableName")]
        public string TableName;
    }

    public class Actions
    {
        [JsonPropertyName("component")]
        public string Component;

        [JsonPropertyName("text")]
        public string Text;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;

        [JsonPropertyName("usetype")]
        public string UseType;
    }

    public class ColumnData
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("value")]
        public string Value;
    }

    public class TableColumn
    {
        [JsonPropertyName("title")]
        public string Title;

        [JsonPropertyName("dataIndex")]
        public string DataIndex;

        [JsonPropertyName("key")]
        public string Key;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("data")]
        public object Data;

        [JsonPropertyName("hideInColumn")]
        public bool? HideInColumn;

        [JsonPropertyName("sorter")]
        public bool? Sorter;

        [JsonPropertyName("mode")]
        public string Mode;

        [JsonPropertyName("actions")]
        public List<Actions> Actions;

        [JsonPropertyName("sql")]
        public string Sql;
    }

    public class TableToolBar
    {
        [JsonPropertyName("component")]
        public string Component;

        [JsonPropertyName("text")]
        public string Text;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("id")]
        public string Id;

        [JsonPropertyName("uri")]
        public string Uri;
    }

    public class BatchToolBar
    {
        [JsonPropertyName("component")]
        public string Component;

        [JsonPropertyName("text")]
        public string Text;

        [JsonPropertyName("type")]
        public string Type;

        [JsonPropertyName("action")]
        public string Action;

        [JsonPropertyName("uri")]
        public string Uri;

        [JsonPropertyName("method")]
        public string Method;

        [JsonPropertyName("usetype")]
        public string UseType;
    }

    public class Layout
    {
        [JsonPropertyName("tableColumn")]
        public List<TableColumn> TableColumn;

        [JsonPropertyName("tableToolBar")]
        public List<TableToolBar> TableToolBar;

        [JsonPropertyName("batchToolBar")]
        public List<BatchToolBar> BatchToolBar;

        /// <summary>
        /// SQL 报表专用
        /// </summary>
        [JsonPropertyName("searchColumn")]
        public List<TableColumn> SearchColumn;
    }

    public class Pivot
    {
        [JsonPropertyName("id")]
        public int Id;

        [JsonPropertyName("admin_id")]
        public int AdminId;

        [JsonPropertyName("group_id")]
        public int GroupId;

        [JsonPropertyName("create_time")]
        public string CreateTime;

        [JsonPropertyName("update_time")]
        public string UpdateTime;

        [JsonPropertyName("delete_time")]
        public object DeleteTime;

        [JsonPropertyName("status")]
        public int Status;
    }

    

    public class Meta
    {
        [JsonPropertyName("total")]
        public int Total;

        [JsonPropertyName("per_page")]
        public int PerPage;

        [JsonPropertyName("page")]
        public int Page;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">主数据表类</typeparam>
    public class PMFApiRoot
    {
        /// <summary>
        /// 页布局
        /// </summary>
        [JsonPropertyName("page")]
        public Page Page;

        /// <summary>
        /// 表布局 含 表头 表尾
        /// </summary>
        [JsonPropertyName("layout")]
        public Layout Layout;

        /// <summary>
        /// 对应table的数据 ， 外键对应data list需要传递。
        /// </summary>
        [JsonPropertyName("dataSource")]
        public object DataSource;

        /// <summary>
        /// 分页信息
        /// </summary>
        [JsonPropertyName("meta")]
        public Meta Meta;
    }




}
