﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PMBF.Core.ViewModels
{
    public class ApiPageModelViewModel
    {
        public class Page
        {
            [JsonPropertyName("title")]
            public string Title;

            [JsonPropertyName("type")]
            public string Type;
        }
        public class Tab
        {
            [JsonPropertyName("name")]
            public string Name;

            [JsonPropertyName("title")]
            public string Title;

            [JsonPropertyName("data")]
            public List<Fields> Data;
        }

        public class Fields
        {
            [JsonPropertyName("title")]
            public string Title;

            [JsonPropertyName("dataIndex")]
            public string DataIndex;

            [JsonPropertyName("key")]
            public string Key;

            [JsonPropertyName("type")]
            public string Type;

            [JsonPropertyName("hideInColumn")]
            public bool HideInColumn;

            [JsonPropertyName("editDisabled")]
            public bool EditDisabled;

            [JsonPropertyName("requiredInForm")]
            public bool RequiredInForm;

            [JsonPropertyName("data")]
            public List<dynamic> Data;

            [JsonPropertyName("sql")]
            public string Sql;

            [JsonPropertyName("isLike")]
            public bool? IsLike;

        }

        public class Action
        {
            [JsonPropertyName("name")]
            public string Name;

            [JsonPropertyName("title")]
            public string Title;

            [JsonPropertyName("data")]
            public List<ActionOper> Data;
        }

        public class ActionOper
        {
            [JsonPropertyName("component")]
            public string Component;

            [JsonPropertyName("text")]
            public string Text;

            [JsonPropertyName("action")]
            public string Action;

            [JsonPropertyName("uri")]
            public string Uri;

            [JsonPropertyName("method")]
            public string Method;

            [JsonPropertyName("type")]
            public string Type;
        }

        public class Layout
        {
            [JsonPropertyName("tabs")]
            public List<Tab> Tabs;

            [JsonPropertyName("actions")]
            public List<Action> Actions;

            public int? colNumb
            {
                get;
                set;
            }
        }

        public class Data
        {
            [JsonPropertyName("page")]
            public Page Page;

            [JsonPropertyName("layout")]
            public Layout Layout;

            [JsonPropertyName("dataSource")]
            public object DataSource;

        }
    }
}
