﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PMBF.Core.ViewModels
{
    public class ApiSQLReportDesignRoot
    {
        [JsonPropertyName("routeName")]
        public string RouteName { get; set; }

        [JsonPropertyName("menuName")]
        public string MenuName { get; set; }

        [JsonPropertyName("sqlText")]
        public string SqlText { get; set; }

        [JsonPropertyName("groupSqlText")]
        public string GroupSqlText { get; set; }
        
        [JsonPropertyName("fields")]
        public List<Field> Fields { get; set; }

        [JsonPropertyName("tableToolbar")]
        public List<TableToolbar> TableToolbar { get; set; }
    }



}
