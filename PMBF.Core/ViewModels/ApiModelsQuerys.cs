﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMBF.Core.ViewModels
{
    public class ApiModelsQuerys
    {
        public bool status { get; set; }

        public int per_page { get; set; }

        public int page { get; set; }

        public string[] sort { get; set; }
        public string[] order { get; set; }

        public string queryfields { get; set; }

        private bool _export = false;

        public bool export
        {
            get
            {
                return _export;
            }
            set
            {
                _export = value;
            }
        }

        private bool _istree = false;

        public bool istree
        {
            get
            {
                return _istree;
            }
            set
            {
                _istree  = value;
            }
        }

        public string treeid { get; set; }

        public string treeparentid { get; set; }

    }
}
