﻿using System.Collections.Generic;

namespace PMBF.Core.ViewModels
{
    public class MenuDataItem
    {
        public string authority { get; set; }

        public List<MenuDataItem> routes { get; set; }

        public bool hideChildrenInMenu { get; set; }

        public bool hideInMenu { get; set; }

        public string icon { get; set; }

        public string locale { get; set; }

        public string name { get; set; }

        public string path { get; set; }
    }
}
