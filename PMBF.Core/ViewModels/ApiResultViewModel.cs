﻿namespace PMBF.Core.ViewModels
{
    public class ApiResultViewModel<T>
    {

        public ApiResultViewModel(T data, bool success, string message, string code="")
        {
            Success = success;
            Message = message;
            Code = code;
            Data = data;
        }

        public bool Success { get; set; }

        public string Message { get; set; }

        public string Code { get; set; }

        public T Data { get; set; }

    }
}
