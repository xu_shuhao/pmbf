﻿using System.Collections.Generic;

namespace PMBF.Core.ViewModels
{
    public class SearchByPageViewModel<T>
    {
        private bool isExport { get; set; }

        public int pageIndex { get; set; }

        public int count { get; set; }

        public int pageSize { get; set; }

        /// <summary>
        /// 查询条件
        /// </summary>
        public T queryParams { get; set; }

        public List<T> data { get; set; }


    }
}
