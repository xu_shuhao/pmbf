﻿using System;
using Furion.FriendlyException;

namespace PMBF.Core.Enums
{
    /// <summary>
    /// 错误码
    /// </summary>
    [ErrorCodeType]
    public enum SystemErrorCodes
    {
        /// <summary>
        /// 用户名或密码不正确
        /// </summary>
        [ErrorCodeItemMetadata("用户名或密码不正确")]
        E0001,

        /// <summary>
        /// 已存在
        /// </summary>
        [ErrorCodeItemMetadata("{0}:{1}已存在！")]
        E0002,


        /// <summary>
        /// 记录不存在
        /// </summary>
        [ErrorCodeItemMetadata("记录不存在")]
        E0003,

        /// <summary>
        /// 账号已存在
        /// </summary>
        [ErrorCodeItemMetadata("账号已存在")]
        E0004,

        /// <summary>
        /// 旧密码不匹配
        /// </summary>
        [ErrorCodeItemMetadata("旧密码不匹配")]
        E0005,

        // <summary>
        /// 指令无法下发
        /// </summary>
        [ErrorCodeItemMetadata("尚有指令待执行，无法下发")]
        E0006,

        // <summary>
        /// 必须选择
        /// </summary>
        [ErrorCodeItemMetadata("请选择{0}")]
        E0007,

        // <summary>
        /// 缺少必要的参数
        /// </summary>
        [ErrorCodeItemMetadata("缺少必要的参数")]
        E0008,

        // <summary>
        /// 无访问权限
        /// </summary>
        [ErrorCodeItemMetadata("无访问权限")]
        E0009,

        // <summary>
        /// 数据库操作错误
        /// </summary>
        [ErrorCodeItemMetadata("数据库操作错误")]
        E0010,

    }
}
