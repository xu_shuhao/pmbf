﻿
using PMBF.Core.ViewModels;

namespace DB.Core.Models
{
    public partial class Model
    {
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public PMFApiRoot ObjectData { get; set; }

    }
}