﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace DB.Core.Models
{
    [Serializable]
    [MetadataType(typeof(UserMeta))]
    public partial class User
    {

        public partial class UserMeta
        {
            /// <summary>
            /// 登录名
            /// </summary>
            /// <example>admin</example>
            public string UserCode { get; set; }

            /// <summary>
            /// 密码
            /// </summary>
            /// <example></example>
            public string Password { get; set; }
        }
        

        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<string> Access { get; set; }

        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<int> userGroup { get; set; }

        //[SqlSugar.SugarColumn(IsIgnore = true)]
        //public string RefreshTokenName { get; set; }
    }
}



