﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Core.Models
{
    public partial class Group
    {
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<int> groupMenus { get; set; }
    }
}
