﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace DB.Core.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("GroupMenu")]
    public partial class GroupMenu
    {
           public GroupMenu(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int GroupID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int MenuRuleID {get;set;}

    }
}
