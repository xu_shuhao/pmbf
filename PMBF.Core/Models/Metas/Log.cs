﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace DB.Core.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("Log")]
    public partial class Log
    {
           public Log(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:日志类型如：登录等其它操作
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LogType {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserCode {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? UserID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:行为内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RequestContent {get;set;}

           /// <summary>
           /// Desc:终端详细信息 如浏览器信息等
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ClientInfo {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OperateIP {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? OperateTime {get;set;}

           /// <summary>
           /// Desc:操作结果
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RequestResult {get;set;}

           /// <summary>
           /// Desc: 终端类型 PC Android  IOS等
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ClientType {get;set;}

    }
}
