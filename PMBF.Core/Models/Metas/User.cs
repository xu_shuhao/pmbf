﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace DB.Core.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("User")]
    public partial class User
    {
           public User(){


           }
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:登录名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UserCode {get;set;}

           /// <summary>
           /// Desc:显示的用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Password {get;set;}

           /// <summary>
           /// Desc:性别
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Sex {get;set;}

           /// <summary>
           /// Desc:头像【key】
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Avatar {get;set;}

           /// <summary>
           /// Desc:签名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Signature {get;set;}

           /// <summary>
           /// Desc:座机
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Telephone {get;set;}

           /// <summary>
           /// Desc:手机
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Mobilephone {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:DateTime.Now
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CreateUserID {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModifyTime {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ModifyUserID {get;set;}

           /// <summary>
           /// Desc:是否进入回收站
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DeleteFlag {get;set;}

           /// <summary>
           /// Desc:删除时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? DeleteTime {get;set;}

           /// <summary>
           /// Desc:删除人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? DeleteUserID {get;set;}

    }
}
