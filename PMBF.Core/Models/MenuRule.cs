﻿using System;
using System.Collections.Generic;

namespace DB.Core.Models
{
    public partial class MenuRule
    {
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<MenuRule> Routes { get; set; }
    }
}
