﻿using System;
using System.Linq;
using PMBF.Core.Handler;
using Furion;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using System.Configuration;
using Furion.Authorization;

namespace PMBF.Core
{
    public class CoreStartup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //Jwt认证
            services.AddJwt<JwtHandler>();

            //数据库
            services.AddSqlSugar(new ConnectionConfig
            {
                //连接符字串
                ConnectionString = App.Configuration["ConnectionStrings:MSSqldb"],
                DbType = DbType.SqlServer,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute//从特性读取主键自增信息
            },
            db =>
            {
                //处理日志事务
                db.Aop.OnLogExecuting = (sql, pars) =>
                {
                    Console.WriteLine(sql);
                    Console.WriteLine(string.Join(",", pars?.Select(it => it.ParameterName + ":" + it.Value)));
                    Console.WriteLine();
                };
            });

            //缓存
            //services.AddEnyimMemcached(options => App.Configuration.GetSection("enyimMemcached").Bind(options));

            //services.AddEnyimMemcached(options =>
            //{
            //options.AddServer("192.168.0.223", 11211);
            //options.AddPlainTextAuthenticator("", "usename", "password");
            //});

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseRouting();

            app.UseCorsAccessor();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEnyimMemcached();

        }
    }
}
