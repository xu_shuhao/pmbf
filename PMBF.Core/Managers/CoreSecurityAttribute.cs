﻿/**
 * 自定义权限参数标签对象
 */
using System;
using Furion.DependencyInjection;

namespace PMBF.Core.Managers

{
    [SkipScan]
    public static class CoreSecurityAttribute
    {
        //自定义角色权限标识
        //action中[SecurityDefine(CoreSecurityAttribute.ViewRoles)]使用。
        public const string ViewRoles = "ViewRoles";
        public const string ViewSecuries = "ViewSecuries";
        public const string GetRoles = "GetRoles";
        public const string InsertRole = "InsertRole";
        public const string GiveUserRole = "GiveUserRole";
        public const string GiveRoleSecurity = "GiveRoleSecurity";
    }
}