﻿using Furion.DependencyInjection;
using Furion.JsonSerialization;
using Newtonsoft.Json;

namespace PMBF.Core.Managers
{
    /// <summary>
    /// Newtonsoft.Json 实现
    /// </summary>
    public class NewtonsoftJsonSerializerProvider : IJsonSerializerProvider, ISingleton
    {
        /// <summary>
        /// 序列化对象
        /// </summary>
        /// <param name="value"></param>
        /// <param name="jsonSerializerOptions"></param>
        /// <returns></returns>
        public string Serialize(object value, object jsonSerializerOptions = null)
        {
            return JsonConvert.SerializeObject(value, jsonSerializerOptions as JsonSerializerSettings);
        }

        /// <summary>
        /// 反序列化字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <param name="jsonSerializerOptions"></param>
        /// <returns></returns>
        public T Deserialize<T>(string json, object jsonSerializerOptions = null)
        {
            return JsonConvert.DeserializeObject<T>(json, jsonSerializerOptions as JsonSerializerSettings);
        }

        public object GetSerializerOptions()
        {
            throw new System.NotImplementedException();
        }
    }
}