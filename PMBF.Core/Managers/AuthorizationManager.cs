﻿/**
 * 自定义权限管理逻辑，方便控制个性化功能（action）权限。
 */
using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace PMBF.Core
{
    /// <summary>
    /// 权限管理器
    /// </summary>
    public class AuthorizationManager : IAuthorizationManager, ITransient
    {
        /// <summary>
        /// 请求上下文访问器
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public AuthorizationManager(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 获取用户Id
        /// </summary>
        /// <returns></returns>
        public int GetUserId()
        {
            //获取设备信息
            //_httpContextAccessor.HttpContext.Request.Headers["User-Agent"] 
            //_httpContextAccessor.HttpContext.User.FindFirstValue("UserId")
            //上面的方法获得登录时自定义的所有数组信息。
            return int.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue("UserId"));
        }

        /// <summary>
        /// 检查权限
        /// </summary>
        /// <param name="resourceId"></param>A
        /// <returns></returns>
        public bool CheckSecurity(string resourceId)
        {
            var userId = GetUserId();

            // ========= 以下代码应该缓存起来 ===========
            // 此处应该查询当前用户所有的后台接口权限，
            // 目前功能权限级别为菜单权限，通过返回菜单控制 暂时不需要
            // 查询用户拥有的权限
            /* var securities = _userRepository
                 .Include(u => u.Roles, false)
                     .ThenInclude(u => u.Securities)
                 .Where(u => u.Id == userId)
                 .SelectMany(u => u.Roles
                     .SelectMany(u => u.Securities))
                 .Select(u => u.UniqueName);

             if (!securities.Contains(resourceId)) return false;*/

            //如需仅限最近用户登录有效，需要判断登录时生成的GUID是否与库中一致，否则提示重新登录。

            return true;
        }
    }
}